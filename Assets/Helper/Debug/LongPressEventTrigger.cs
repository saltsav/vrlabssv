﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
 
public class LongPressEventTrigger : UIBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {
    private float durationThreshold = 2.0f;

    public UnityEvent onLongPress = new UnityEvent();

    private bool isPointerDown = false;
    private bool longPressTriggered = false;
    private float timePressStarted;


    private void Update () {
        if(isPointerDown && !longPressTriggered) {
            if(Time.time - timePressStarted > durationThreshold) {
                longPressTriggered = true;
                onLongPress.Invoke();
            }
        }
    }

    public void OnPointerDown (PointerEventData eventData) {
        timePressStarted = Time.time;
        isPointerDown = true;
        longPressTriggered = false;
    }

    public void OnPointerUp (PointerEventData eventData) {
        isPointerDown = false;
    }


    public void OnPointerExit (PointerEventData eventData) {
        isPointerDown = false;
    }
}